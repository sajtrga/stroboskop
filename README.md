# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://sajtrga@bitbucket.org/sajtrga/stroboskop.git
git add README.md
git commit
git push
```

Naloga 6.2.3:
https://bitbucket.org/sajtrga/stroboskop/commits/4a1cd33646880f73406bf199b5274f443489b3ff

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/sajtrga/stroboskop/commits/2af9bd238c2d79da889942e88a9de0e5321ef4e8

Naloga 6.3.2:
https://bitbucket.org/sajtrga/stroboskop/commits/e1ac35070f02b3fac3a5bcc296741130bdd59e3e

Naloga 6.3.3:
https://bitbucket.org/sajtrga/stroboskop/commits/5f3d2a971957bac25bfb3813f4684a8c4f0838db

Naloga 6.3.4:
https://bitbucket.org/sajtrga/stroboskop/commits/48a0ebd2f0ae70f622d94ff35eb27b5a3b4431f7

Naloga 6.3.5:

```
git checkout master
git merge izgled
git commit
git push
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/sajtrga/stroboskop/commits/2ca62647a952118bbf7ea01f28340308d8a56115

Naloga 6.4.2:
https://bitbucket.org/sajtrga/stroboskop/commits/7e0b496704e49205f686caa829ec2b2b7ac9ec7f

Naloga 6.4.3:
https://bitbucket.org/sajtrga/stroboskop/commits/49cc5adbc558e95dbf8f75be9e4055f41ed2eaff

Naloga 6.4.4:
https://bitbucket.org/sajtrga/stroboskop/commits/59a65143b0e599742978bc727f2ae9d303c16208
